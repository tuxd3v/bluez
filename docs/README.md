# bluez
---

Patches for bluez package

#### Table of contents
* [Introduction](#introduction)
* [Characteristics](#characteristics)
* [Archs](#archs)
* [Testing](#testing)
* [Credits](#credits)

### Introduction:
----
This pacthes will be, automatically, applied when you build the package 'bluez' from sources..
If you want to apply patches by hand:
   ```lua
   # jump to bluez directory
   cd bluez
   patch -p1 < debian/patches/the_patch_you_want_to_apply.patch
   ```
But you don't need to do this, since when building the package they get picked up,
And the bluez source code is patched automatically..

### Characteristics:
----
* Add support for Cypress(vendor ID 305) devices CYW43{143,438,455} as Brodcomm devices..
* Devuanize bluez package, since there are some hardcoded places( Texas intrument Alt, Qualcomm, Broadcomm ) in **tools** directory that look for firmware in **/etc/firmware**, instead of **/lib/firmware**.

### Archs:
----
This paches are Architecture independent, they should be built for any arch were you want support for those Bluethoot devices..

### Testing:
----
Tests for Cypress devices were done, with the help of c0rnelious running on Raspberry Pi hardware( RPi Bluetooth is from Cypress ) ..

### Credits
----
patches       : tuxd3v, others

